package com.example.tyuip.reservations;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class HttpRequest extends AppCompatActivity {
    public String token;
    public String build;
    public ArrayList<ReservationObject> reservations;
    int roomId = 43576;

    public HttpRequest(String roomId){
        this.roomId = Integer.parseInt(roomId);
    }

    //instantiates reservation array list
    public void start(){
        reservations = new ArrayList<>();
    }


    //recieves form-id and token id to fill in the form
    public int getToken(ReservationObject reservation, String cookies) {
        try {
            //Your server URL
            String loginUrl = "https://rooms.library.upei.ca/node/add/room-reservations-reservation/"+reservation.date+"/"+reservation.startTimeArmy+"/"+ roomId;

            URL obj = new URL(loginUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("COOKIE", cookies);

            con.setRequestMethod("POST");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + loginUrl);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            int tokenInt = response.toString().indexOf("token");
            token = response.toString().substring(tokenInt + 14);
            tokenInt = token.indexOf('"');
            token = token.substring(0,tokenInt);
            System.out.println("DEBUG -TOKEN: "+token);

            int buildInt = response.toString().indexOf("form_build_id");
            build = response.toString().substring(buildInt + 22);
            buildInt = build.indexOf('"');
            build = build.substring(0,buildInt);
            System.out.println(build);

            return connectPost(reservation, cookies);
        }catch(Exception e) {
            System.err.println("Error: "+e);
        }
        return 0;

    }

    //Sends post request to the URL of the reservation made by the user
    public int connectPost(ReservationObject reservation, String cookies){
            try{
                String url = "https://rooms.library.upei.ca/node/add/room-reservations-reservation/"+reservation.date+"/"+reservation.startTimeArmy+"/"+ roomId;

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestProperty("COOKIE", cookies);

                con.setRequestMethod("POST");

                String urlParameters = "title="+reservation.title+"&changed=&form_build_id="+build+"&form_token="+token+"&form_id=room_reservations_reservation_node_form&reservation_length[und]="+reservation.length+
                        "&additional_settings__active_tab=&op=Save";
                if(reservation.privacy==1){
                    urlParameters.concat("&reservation_block_title[und]=1");
                }

                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();


                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                return responseCode;

            }catch(Exception e){
                System.err.println("Error: "+e);
            }
            return 0;
        }

    //gets all schedule hours from the endpoint for room hours as a JSON file
    public ScheduleObject getScheduleHours(String date){
        try {
            System.out.println("DATE: " + date);
            //Your server URL
            String url = "https://rooms.library.upei.ca/room_reservations/hours";

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/47.0.2526.106 Chrome/47.0.2526.106 Safari/537.36");


            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println(response.toString());
            return parseScheduleJson(response.toString(), date);
        }catch(Exception e)
        {
            System.err.println(e);

        }
        return null;
    }

    //Gets all reservations for a single room and date as a JSON file
    public void getReservations(String date) {
        try {
            String day = date.substring(8,10);
            String month = date.substring(5,7);
            String year = date.substring(0,4);
            if (day.charAt(0) == '0')
                day = day.substring(1);
            if (month.charAt(0) == '0')
                month = month.substring(1);
            //roomId = Integer.parseInt(readFromFile());

            //Your server URL
            String url = "https://rooms.library.upei.ca/rest/reservations?date[value][month]="+month+"&date[value][day]="+day+"&date[value][year]="+year+"&room="+ roomId;

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/47.0.2526.106 Chrome/47.0.2526.106 Safari/537.36");


            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println(response.toString());
            parseReseravtionJson(response.toString());
        }catch(Exception e)
        {
            System.err.println(e);

        }
    }

    //Parses JSON to create indivual reservations and puts them into the array list
    private void parseReseravtionJson(String str){
        try {
            boolean more = true;
            int i = 0;
            while(more) {
                JSONArray obj = new JSONArray(str);
                String title = obj.getJSONObject(i).getString("title");
                String date = obj.getJSONObject(i).getString("Date");
                String temp = obj.getJSONObject(i).getString("Length");
                temp = temp.substring(0, temp.indexOf(" "));
                int length = Integer.parseInt(temp);
                int time = obj.getJSONObject(i).getInt("Time");
                int privacy = obj.getJSONObject(i).getInt("Private");
                ReservationObject reservation = new ReservationObject(title, date, length, time, privacy);
                reservations.add(reservation);
                i++;
            }
        }catch(Exception e){
            System.err.println(e);
        }

    }

    public void setRoomId(String id){
        roomId = Integer.parseInt(id);
    }

    //Parses json to create indivual schedule objects and returns them to getScheduleHours
    private ScheduleObject parseScheduleJson(String str, String date){
        try {
            JSONObject obj = new JSONObject(str);
            date = date.substring(0,10);
            obj = obj.getJSONObject(date);
            int openTime = obj.getInt("first_shift_open");
            int closeTime = obj.getInt("first_shift_close");
            boolean open = obj.getBoolean("open");
            ScheduleObject scheduleObject = new ScheduleObject(openTime, closeTime, open, date);
            return  scheduleObject;

        }catch(Exception e){
            System.err.println(e);
            ScheduleObject scheduleObject = new ScheduleObject(800,2300,true,"1994-07-26");
            return scheduleObject;
        }
    }

}