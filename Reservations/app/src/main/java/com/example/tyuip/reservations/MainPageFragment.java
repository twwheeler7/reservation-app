package com.example.tyuip.reservations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;


public class MainPageFragment extends DialogFragment {

    //initilize some variables
    TextView textView;
    public ArrayList<String> mainList;
    CustomAdapter adapter;
    public static String EXTRA_INT = "pos";
    public static String AMOUNT = "amount";
    public static String DATE = "date";
    public HttpRequest http;
    public ListView listView;
    String roomNumber;
    String date;
    View view;
    ScheduleObject schedule;
    ConverterObject converter;
    Runnable runnable;

    //setup our list of times to be displayed for slots
    public static final String[] timeSlots = new String[]{"8:00AM", "8:30AM", "9:00AM",
            "9:30AM", "10:00AM", "10:30AM", "11:00AM", "11:30AM", "12:00PM", "12:30PM", "1:00PM",
            "1:30PM", "2:00PM", "2:30PM", "3:00PM", "3:30PM", "4:00PM", "4:30PM", "5:00PM", "5:30PM",
            "6:00PM", "6:30PM", "7:00PM", "7:30PM", "8:00PM", "8:30PM", "9:00PM", "9:30PM", "10:00PM", "10:30PM", "11:00PM"};

    String[] rooms = {"260", "261","302","303","306", "330", "331", "332", "333", "334", "335", "336", "341", "342", "344", "345", "346", "347", "348", "349", "350"};
    String[]  roomIds = {"63",  "65", "67", "69", "71",  "73",  "75",  "77",  "79",  "81",  "83",  "85",  "87",  "89",  "91",  "93",  "95",  "97",  "99",  "101", "103"};


    //simply calling super onCreate, doing nothing special at the moment
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //same as above, nothing special at the moment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mainpage_list, container, false);
    }


    //When the list fragment is created run the following
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        //setup our mainList with timeslots
        super.onActivityCreated(savedInstanceState);
        mainList = new ArrayList<>(50);
        //Create our HttpRequest to read in the schedule
        String temp = readFromFile();
        if (temp.equals("")) {
            temp = readFromFile();
            roomNumber = findRoomNumber(temp);
            http = new HttpRequest(temp);
        }
        else
            roomNumber = findRoomNumber(temp);
            http = new HttpRequest(temp);
        converter = new ConverterObject();
        ((MainActivity) getActivity()).setActionBarTitle("Book reservation for: Room "+roomNumber);
        http.start();
        listView = (ListView) getActivity().findViewById(R.id.sitesList);
        view = getActivity().findViewById(R.id.listLayout);
        textView = (TextView) getActivity().findViewById(R.id.textDate);
        date = textView.getText().toString();
        date = converter.convertDateWordsToFull(date);
        runnable = new Runnable(){
            @Override
            public void run(){
                http.setRoomId(readFromFile());
                schedule = http.getScheduleHours(date);
                http.getReservations(date);
            }
        };
        Thread worker = new Thread(runnable);
        worker.start();
        try{
            worker.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        textView.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                date = textView.getText().toString();
                date = converter.convertDateWordsToFull(date);
                http.start();
                Thread textChange = new Thread(runnable);
                textChange.start();
                try{
                    textChange.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                fillSchedule();
            }
        });

        adapter = new CustomAdapter(getActivity(), R.layout.fragment_mainpage, mainList);
        listView.setAdapter(adapter);
        fillSchedule();



        //if a timeslot is pressed, go to booking page assuming it is free
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int i =0;
                for(i=position;i<position+6;i++){
                    if(i>=mainList.size()){
                        break;
                    }
                    else if(mainList.get(i).length()>9){
                        break;
                    }
                }
                if(mainList.get(position).length()<9&&!mainList.get(position).equals("Not open")) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.putExtra(EXTRA_INT, timeSlots[position]);
                    intent.putExtra(AMOUNT,i-position);
                    intent.putExtra(DATE, textView.getText().toString());
                    startActivity(intent);

                }
            }
        });
    }

    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = getActivity().openFileInput("config.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
            createFile();
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public void createFile(){
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getActivity().openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write("63");
            outputStreamWriter.close();
            getActivity().finish();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public String findRoomNumber(String room){
        for(int i=0;i<rooms.length;i++){
            if(roomIds[i].toString().equals(room))
                room = rooms[i].toString();
        }
        return room;
    }

    //this is where timeslots are filled with reservation objects
    public void fillSchedule(){
        int tempI = 0;
        int q = 0;
        mainList.clear();
        for(int i =schedule.openTime;i<=schedule.closeTime-50;i+=50) {
            if(String.valueOf(i).contains("50")&&(!String.valueOf(i).equals("1500"))) {
                tempI = i - 20;
            }
            else
                tempI = i;
            mainList.add(converter.convertTimeArmyToStandard(String.valueOf(tempI)));
            timeSlots[q] = converter.convertTimeArmyToStandard(String.valueOf(tempI));
            q++;
        }

        for(int i = 0;i<http.reservations.size();i++){
            for(int j = 0;j<mainList.size();j++){
                int x = 0;
                if(timeSlots[j].equals(http.reservations.get(i).timeToString())&&http.reservations.get(i).date.equals(converter.convertDateFullToSimple(date))){
                    while(x*30 < http.reservations.get(i).length){
                        String temp = mainList.get(j+x) + "      -      " + http.reservations.get(i).title;
                        mainList.set(j+x, temp);
                        System.out.println(mainList.get(j+x));
                        x++;
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

}