package com.example.tyuip.reservations;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Handler handler;
    final static int TIME_TO_WAIT = 60000;      // TODO  change time back to 60000 after testing

    private WebView mWebView;   //creates Webview
    private String cookies;
    Bundle extras;

    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        extras = getIntent().getExtras();

        //creates the link to webview
        mWebView = (WebView) findViewById(R.id.login_webview);
        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebView.loadUrl("https://rooms.library.upei.ca/Shibboleth.sso/Login?target=https%3A%2F%2Frooms.library.upei.ca%2Froom_reservations%2Flist");

        //Force all urls to be loaded by Webview instead of default browser
        mWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                cookies = CookieManager.getInstance().getCookie(url);
                System.out.println("All the cookies in a string:" + cookies);
                if (cookies.toLowerCase().contains("shib".toLowerCase())) {
                    handler.removeCallbacks(runnable);
                    Intent i = new Intent(LoginActivity.this, BookingPage.class);
                    i.putExtra("COOKIE", cookies);
                    i.putExtras(extras);
                    startActivity(i);
                    CookieManager.getInstance().removeAllCookie();
                    finish();
                }
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handler = new Handler();
        handler.postDelayed(runnable, TIME_TO_WAIT);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // Allow WebView to respond to back button instead of exiting
    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onUserInteraction(){
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, TIME_TO_WAIT);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if(mWebView != null) {
                Toast.makeText(LoginActivity.this, "Returning to schedule due to inactivity - Login", Toast.LENGTH_SHORT).show();
                destroyWebView();
           }
        }
    };

    // code to destroy the webview
    private void destroyWebView() {
        mWebView.removeAllViews();

        if(mWebView != null){

            Intent intent = new Intent(LoginActivity.this, ScreenSaverActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);    // clears activity stack
            startActivity(intent);

            mWebView.loadUrl("about:blank");
            mWebView.destroy();
        }
    }

}
