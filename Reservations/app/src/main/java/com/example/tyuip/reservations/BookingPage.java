package com.example.tyuip.reservations;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BookingPage extends AppCompatActivity {

    String pos;
    Handler handler;
    final int TIME_TO_WAIT = 30000;
    int amount;
    String date;
    String cookies;
    String response = "Reservation successfully created";
    HttpRequest http;
    ReservationObject reservation;
    ConverterObject converter;

    //onCreate get position/starting time and setup our display
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        converter = new ConverterObject();
        handler = new Handler();
        handler.postDelayed(runnable, TIME_TO_WAIT);
        pos = getIntent().getStringExtra(MainPageFragment.EXTRA_INT);
        amount = getIntent().getIntExtra(MainPageFragment.AMOUNT, amount);
        date = getIntent().getStringExtra(MainPageFragment.DATE);
        cookies = getIntent().getStringExtra("COOKIE");

        setContentView(R.layout.activity_booking_page);
        createRadioGroup();
        setDate();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Create Room Reservation");

        http = new HttpRequest(readFromFile());
        http.start();

    }

    //resets handler when user interacts with screen
    @Override
    public void onUserInteraction(){
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, TIME_TO_WAIT);
    }


    //display the correct starting time
    protected void setDate(){
        final TextView reservationTime = (TextView)findViewById(R.id.reservationTime);
        reservationTime.setText("Time: " + pos);
        final TextView reservationDate = (TextView)findViewById(R.id.reservationDate);
        reservationDate.setText("Date: "+date);
    }

    //Creates appropriate amount of radio buttons
    protected void createRadioGroup(){
        ViewGroup radioButtons = (ViewGroup) findViewById(R.id.reserveLength);
        for(int i =0; i< amount;i++){
            RadioButton button = new RadioButton(this);
            button.setId(i);
            button.setText((i+1)*30 + " minutes");
            radioButtons.addView(button);
        }
    }

    //returns to schedule after certain time
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(BookingPage.this, "Returning to schedule due to inactivity - Booking", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(BookingPage.this, ScreenSaverActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    };

    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("config.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    //send our reservation
    public void sendReservation(View button) {
        try {
            final EditText titleField = (EditText) findViewById(R.id.EditTextName);
            String title = titleField.getText().toString();
            if(title.equals(""))
                throw new Exception("Title is required");
            final CheckBox privateBox = (CheckBox) findViewById(R.id.CheckBoxResponse);
            boolean privateBool = privateBox.isChecked();
            int privacy = 0;
            if (privateBool)
                privacy = 1;


            final RadioGroup reservationLength = (RadioGroup) findViewById(R.id.reserveLength);
            int selectedId = reservationLength.getCheckedRadioButtonId();
            final RadioButton lengthButton = (RadioButton) findViewById(selectedId);
            String lengthS = lengthButton.getText().toString();
            int length = Integer.parseInt(lengthS.replaceAll("[\\D]", ""));

            reservation = new ReservationObject(title, converter.convertDateWordsToFull(date), length, converter.convertTimeStandardToArmy(pos), privacy);

            new ConnectionPost().execute();

        }catch(Exception e){
            AlertDialog alertDialog = new AlertDialog.Builder(BookingPage.this).create();
            alertDialog.setTitle("Alert");
            response = "Required field missing";
            alertDialog.setMessage(response);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
            alertDialog.show();
        }

    }

    //Alert that changes depending on if reservation was sent or not
    private void alert(boolean success){
        AlertDialog alertDialog = new AlertDialog.Builder(BookingPage.this).create();
        if(success) {
            alertDialog.setTitle("Success");
            alertDialog.setMessage(response);
        }
        else {
            alertDialog.setTitle("Failed");
            response = "Reservation failed to create.  You may only have one reservation per day, and 4 total.";
            alertDialog.setMessage(response);
        }
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(BookingPage.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }

    //uses httpRequest to send post containing reservation
    private class ConnectionPost extends AsyncTask<Object, Object, Void> {

        @Override
        protected Void doInBackground(Object... arg0) {
            http.setRoomId(readFromFile());
            http.getToken(reservation, cookies);
            return null;
        }

        @Override
        protected void onPostExecute(Void v){
            new ConnectionGet().execute();
        }
    }

    //uses httpRequest to send get containing all reservations on the site
    private class ConnectionGet extends AsyncTask<Object, Object, Void> {

        @Override
        protected Void doInBackground(Object... arg0) {
            http.setRoomId(readFromFile());
            http.getReservations(converter.convertDateWordsToFull(date));
            for(int i=0;i<http.reservations.size();i++)
                System.out.println("Reservation i: "+http.reservations.get(i).title);
            return null;
        }

        @Override
        protected void onPostExecute(Void v){
            if(http.reservations.contains(reservation)){
                alert(true);
            }
            else
                alert(false);
        }
    }



}
