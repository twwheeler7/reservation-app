package com.example.tyuip.reservations;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class CustomAdapter extends ArrayAdapter<String> {
    int layoutResourceId;
    ArrayList data;
    Context context;
    private static final int TYPE_COUNT = 2;
    private static final int TYPE_ITEM_COLORED = 1;
    private static final int TYPE_ITEM_NORMAL = 0;

    public CustomAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {

        String item = getItem(position).toString();

        return (item.length()>7) ? TYPE_ITEM_COLORED : TYPE_ITEM_NORMAL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(layoutResourceId, parent, false);

        }
        TextView title = (TextView) convertView.findViewById(R.id.title);
        title.setText(data.get(position).toString());
        switch (getItemViewType(position)) {
            case TYPE_ITEM_COLORED:
                convertView.setBackgroundColor(Color.GRAY);
                break;
            case TYPE_ITEM_NORMAL:
                break;
        }
        return convertView;

    }

}