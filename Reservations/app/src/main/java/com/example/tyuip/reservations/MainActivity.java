package com.example.tyuip.reservations;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    final int TIME_TO_WAIT = 60000;

    Handler handler;
    ConverterObject converter;

    //on creation of the main page. runs on startup of the app
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        converter = new ConverterObject();
        final Calendar c = Calendar.getInstance();
        TextView textDate = (TextView) findViewById(R.id.textDate);
        textDate.setText(converter.convertDateCustomToWords(c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH) + 1, c.get(Calendar.YEAR)));
        Button button = (Button) findViewById(R.id.today);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                setDateToToday();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        handler = new Handler();
        handler.postDelayed(runnable, TIME_TO_WAIT);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, RoomChangingActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //Show day picker when button is pressed
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        FragmentManager fm = getFragmentManager();
        newFragment.show(fm, "datePicker");
    }

    //resets the handler if the user interacts with the screen
    @Override
    public void onUserInteraction(){
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, TIME_TO_WAIT);
    }

    //Sets date back to current date
    public boolean setDateToToday(){
        final TextView textView = (TextView) this.findViewById(R.id.textDate);
        boolean set = false;
        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        System.out.println(textView.getText());
        if(!dateFormat.format(date).equals(textView.getText())) {
            set = true;
            textView.setText(dateFormat.format(date));
        }
        System.out.println(set);
        return set;

    }

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }


    //calls setDateToToday and notifys the user if the date is changed
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(MainActivity.this, ScreenSaverActivity.class);
            startActivity(intent);
        }
    };

    //setup the day picker
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        final Calendar c = Calendar.getInstance();
        ConverterObject converter = new ConverterObject();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }


        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            final TextView textView = (TextView) getActivity().findViewById(R.id.textDate);
            month+=1;
            textView.setText(converter.convertDateCustomToWords(day, month, year));
            this.year = year;
            this.month = month;
            this.day = day;
        }

    }


}