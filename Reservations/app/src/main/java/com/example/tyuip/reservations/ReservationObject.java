package com.example.tyuip.reservations;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//Reservation object. when all information is read in through our http request
//Things are stored in a reservation object for easily converted times and
//to simplify filling in the schedule with objects.
public class ReservationObject {

    //setup our variables
    String title;
    String privateTitle;
    String date;
    ConverterObject converter;
    Boolean am = true;
    int privacy = 0;
    int length;
    int startTimeArmy;
    String startTime;


    public ReservationObject(String title, String date, int length, int time, int privacy){
    //constructor which sets our variables and calls our timeConvert
        converter = new ConverterObject();
        this.title = title;
        this.date = converter.convertDateFullToSimple(date);
        this.length = length;
        this.startTime = Integer.toString(time);
        startTimeArmy = time;
        if(privacy==1) {
            this.privacy = privacy;
            privateTitle = title;
            this.title = "Booked";
        }
        timeConvert();
    }

    //convert our time from 24 hour
    private void timeConvert(){
        startTime = converter.convertTimeArmyToStandard(startTime);
    }

    //format our time to look pretty after converting from 24 hour
    public String timeToString(){
        return startTime;
    }

    @Override
    public boolean equals(Object object){
        ReservationObject reservation = (ReservationObject) object;
        return (reservation.startTime.equals(startTime) && reservation.date.equals(date) && reservation.title.equals(title));
    }




}