package com.example.tyuip.reservations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RoomChangingActivity extends AppCompatActivity {

    final int PIN = 1234;
    String[] rooms = {"260", "261","302","303","306", "330", "331", "332", "333", "334", "335", "336", "341", "342", "344", "345", "346", "347", "348", "349", "350"};
    String[]  roomIds = {"63",  "65", "67", "69", "71",  "73",  "75",  "77",  "79",  "81",  "83",  "85",  "87",  "89",  "91",  "93",  "95",  "97",  "99",  "101", "103"};
    List roomsList = new ArrayList<>(Arrays.asList(rooms));
    String data;
    int id;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_check);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Change Room Number");
    }

    public void checkPin(View view){
        EditText pinField = (EditText) findViewById(R.id.adminPIN);
        if(Integer.parseInt(pinField.getText().toString()) == PIN) {
            setContentView(R.layout.activity_room_change);
            spinner = (Spinner) findViewById(R.id.spinner);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    data = spinner.getSelectedItem().toString();
                    id = roomsList.indexOf(data);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }

            });
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, roomsList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }
    }

    public void changeRoom(View view) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(roomIds[id]);
            outputStreamWriter.close();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


}

