package com.example.tyuip.reservations;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tyuip on 17/03/16.
 */
public class ConverterObject {

    public ConverterObject(){

    }

    public String convertTimeArmyToStandard(String time) {
        DateFormat readFormat = new SimpleDateFormat("HHmm");
        DateFormat writeFormat = new SimpleDateFormat("h:mmaa");
        Date date = null;

        try {
            if (time.length() < 4)
                time = "0".concat(time);
            date = readFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            String formattedDate = writeFormat.format(date);
            String newTime = formattedDate;
            return newTime;

        }
        return null;
    }

    public String convertDateFullToSimple(String dateTemp){
        DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat writeFormat = new SimpleDateFormat("MM/dd");
        Date date = null;
        try
        {
            date = readFormat.parse(dateTemp);
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }
        if( date != null )
        {
            return writeFormat.format( date );

        }
        return null;
    }

    public String convertDateWordsToFull(String dateTemp){
        DateFormat readFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try
        {
            date = readFormat.parse(dateTemp);
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }
        if( date != null )
        {
            return writeFormat.format( date );

        }
        return null;
    }

    public String convertDateCustomToWords(int day, int month, int year){
        String time = day+"-"+month+"-"+year;
        DateFormat readFormat = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat writeFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        Date date = null;
        try
        {
            date = readFormat.parse(time);
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }
        if( date != null )
        {
            return writeFormat.format( date );

        }
        return null;
    }

    public int convertTimeStandardToArmy(String time){
        DateFormat readFormat = new SimpleDateFormat("hh:mmaa");
        DateFormat writeFormat = new SimpleDateFormat("HHmm");
        Date date = null;

        try
        {
            date = readFormat.parse(time);
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }
        if( date != null )
        {
            String formattedDate = writeFormat.format( date );
            int newTime = Integer.parseInt(formattedDate);
            return newTime;

        }
        return 0;
    }

}
