package com.example.tyuip.reservations;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tyuip on 15/03/16.
 */
public class ScheduleObject {
    int openTime;
    int closeTime;
    boolean open;
    String date;

    public ScheduleObject(int openTime, int closeTime, boolean open, String date){
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.open = open;
        this.date = date;
    }

}
